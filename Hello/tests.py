from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve
from .views import index, statRemove
from .models import Stat
from .forms import StatForm
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class UnitTest(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/home/')
        self.assertEqual(response.status_code, 200)

    def test_using_index_func(self):
        found = resolve('/home/')
        self.assertEqual(found.func, index)
        
    def test_model_can_create_new_Stat(self):
        # Creating a new activity
        new_activity = Stat.objects.create(status='mengerjakan lab ppw')

        # Retrieving all available activity
        counting_all_available_Stat = Stat.objects.all().count()
        self.assertEqual(counting_all_available_Stat, 1)

    def test_form_Stat_input_has_placeholder_and_css_classes(self):
        form = StatForm()
        self.assertIn('class="form-control"', form.as_table())
        self.assertIn('id="id_status"', form.as_table())

    def test_form_validation_for_blank_items(self):
        form = StatForm(data={'status':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'],
            ["This field is required."]
        )
    def test_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/home/', {'status': test})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/home/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/home/', {'status': ''})
        self.assertEqual(response_post.status_code, 200)

        response= Client().get('/home/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_post_success_and_delete(self):
        new_activity = Stat.objects.create(status='mengerjakan lab ppw')
        response_post = Client().post('/statRemove/1')
        
        counting_all_available_Stat = Stat.objects.all().count()
        self.assertEqual(counting_all_available_Stat, 0)
        

class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--window-size=1420,1080')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.selenium  = webdriver.Chrome(executable_path='./chromedriver', chrome_options=chrome_options)

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()

    def test_input_todo(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get(self.live_server_url)
        # find the form element
        status = selenium.find_element_by_id('id_status')

        submit = selenium.find_element_by_id('submit')

        # Fill the form with data
        status.send_keys('Mengerjakan Lab PPW')

        # submitting the form
        submit.send_keys(Keys.RETURN)
