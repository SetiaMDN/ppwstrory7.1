from django.shortcuts import render, redirect
from .forms import StatForm
from .models import Stat
from . import forms
from datetime import datetime, date

# Create your views here.
def index(request):
    if request.method=='POST':
        form=StatForm(request.POST)
        if form.is_valid():
            status=form.cleaned_data['status']
            stat = Stat(status=status)
            stat.save()
            #Stat.objects.all().delete()
            return redirect('index')
        else:
            return render(request, "index.html",{'form':form})
    form=StatForm()
    forms=Stat.objects.all().order_by('date')
    return render(request, "index.html",{'form':form, 'forms':forms})

def statRemove(request, stat_id):
    stat=Stat.objects.get(id=stat_id)
    stat.delete()
    return redirect('index')